import React from 'react';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';
import axios from 'axios';


export default class App extends React.Component {
  constructor (props) {
    super(props);
    this._handleInput = this._handleInput.bind(this);
    this.mostrarWS = this.mostrarWS.bind(this);
    this.state = {
      data: undefined,
      currentId: 1
    }
  }

  _handleInput(e) {
    console.log(e);
    if(e && !isNaN(e)) {
      this.setState({currentId : e})
      console.log(e);
    }
  }

  mostrarWS () {
    if (this.state.data) {
      this.setState({data: undefined});
    } else {
    axios.get(`https://jsonplaceholder.typicode.com/users/${this.state.currentId}`)
      .then((json) => { 
        this.setState((prevState) => {
          return { data: json.data }
        })
      })
      .catch((error) => { this.setState({data: error})});
    } 
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.mainText}>SISA App</Text>
        <TextInput 
        style={styles.inputStyle} 
        onChangeText={this._handleInput} autoFocus={true} placeholderTextColor="blue" placeholder="Ingrese ID"></TextInput>
        <Button style={styles.btnWS} onPress={this.mostrarWS} title={ !this.state.data ? "Test WS" : "Limpiar" } />
        <Text style={styles.responseStyle}>{ this.state.data ? JSON.stringify(this.state.data, null, 4) : "" }</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mainText: {
    fontSize: 30,
    color: 'blue',
    fontWeight: 'bold'
  },
  btnWs: {
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: 'blue'
  },
  inputStyle: {
    minHeight: 10,
    backgroundColor: 'yellow'
  }, 
  responseStyle: {
    margin: 20,
    fontSize: 12
  }
});
